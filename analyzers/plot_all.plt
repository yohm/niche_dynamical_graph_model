set term png
set output "num_species.png"
plot "_input/num_species.dat" w l

set output "distance.png"
plot "_input/distance.dat" w l

set output "indegree.png"
plot "_input/indegree.dat" w l

set logscale
set output "lifetime_logbin.png"
plot "_input/lifetime_logbin.dat" w lp
unset logscale

set output "positions.png"
plot "_input/positions.dat" w d

