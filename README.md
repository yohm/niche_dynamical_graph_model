An enhancement of dynamical graph model.

# Model description

## Migration rule
- Randomly pick a point where a new species locates
- Make interactions with species within radius r with probability c.
    - For each interaction, a random number drawn from a  Gaussian distribution is assigned.

## Extinction
- Same as the original dynamical graph model.

# Usage
- install dependent gems as follows
    ```
    bundle install
    ```
- run the simulator
    ```
    bundle exec ruby sim.rb
    ```
 
