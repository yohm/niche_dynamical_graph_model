require 'pp'
require 'logger'
require 'json'
require 'ruby-progressbar'

class Simulator

  attr_reader :system_size, :interaction_radius, :initial_num_species,
              :lifetime_hist, :indegree_hist, :distance_hist

  def initialize(parameters)
    @lifetime_hist = {}
    @indegree_hist = {}
    @distance_hist = {}
    @time = 0
    set_paramaeter(parameters)
    prepare_initial_state
  end

  def update
    migration
    selection
    @time += 1
  end

  def num_species
    @all_species.size
  end

  def positions
    @all_species.map(&:position)
  end

  def count_up_indegree_hist
    @all_species.each do |s|
      i = s.indegree
      @indegree_hist[i] = @indegree_hist[i].to_i + 1
    end
  end

  def count_up_distance_hist
    @num_bin ||= 100
    @bin_size ||= @system_size / @num_bin
    @all_species.combination(2).each do |s1, s2|
      distance = s1.distance_to(s2)
      key = (distance/@bin_size).to_i
      @distance_hist[key] = @distance_hist[key].to_i + 1
    end
  end

  private
  def set_paramaeter(parameters)
    @system_size = parameters[:system_size]
    @interaction_radius = parameters[:interaction_radius]
    @initial_num_species = parameters[:initial_num_species]
    @connectance = parameters[:connectance]
    if @system_size.nil? || @interaction_radius.nil? || @initial_num_species.nil? || @connectance.nil?
      raise "invalid input parameters"
    end
    Species.set_paramaeters(@connectance, @interaction_radius, @system_size)
  end

  def prepare_initial_state
    @all_species = []
    @initial_num_species.times do |i|
      migration
    end
    selection
    @time = 0
  end

  def migration
    s = Species.new(@time)
    interactable_species = @all_species.find_all {|existing| existing.is_close_to?(s) }
    s.make_interactions_with(interactable_species)
    @all_species.push s
  end

  def selection
    unfit_species = @all_species.min_by {|s| s.fitness }
    minimum_fitness = unfit_species.fitness
    while minimum_fitness <= 0.0 && @all_species.size > 1
      # species with zero fitness goes extinct when number of species is larger than 10
      # when number of species is less than 10, species with zero fitness can survive (incubation rule)
      @all_species.delete(unfit_species)
      @all_species.each do |s|
        s.remove_interaction_with(unfit_species)
      end
      # count lifetime distribution
      lifetime = @time - unfit_species.migrated_at
      @lifetime_hist[lifetime] = @lifetime_hist[lifetime].to_i + 1 if lifetime > 0

      raise "No species" if @all_species.empty?
      unfit_species = @all_species.min_by {|s| s.fitness}
      minimum_fitness = unfit_species.fitness
    end
  end

  class Species

    def self.set_paramaeters(connectance, interaction_radius, system_size)
      @@connectance = connectance
      @@interaction_radius = interaction_radius
      @@system_size = system_size
    end

    attr_reader :position, :migrated_at

    def initialize(current_time)
      @interactions = {}
      @position = [rand * @@system_size, rand * @@system_size]
      @migrated_at = current_time
    end

    def make_interactions_with(species)
      species.each do |s|
        if rand < @@connectance
          s.make_new_interaction(self, gaussian)
        end
        if rand < @@connectance
          self.make_new_interaction(s, gaussian)
        end
      end
    end

    def make_new_interaction(species, strength)
      @interactions[species.__id__] = strength
    end

    def remove_interaction_with(species)
      @interactions.delete(species.__id__)
    end

    def is_close_to?(other)
      distance_to(other) <= @@interaction_radius
    end

    def fitness
      @interactions.values.inject(0.0, :+)
    end

    def indegree
      @interactions.size
    end

    def distance_to(other)
      dx = (other.position[0] - self.position[0]).abs
      dx = @@system_size - dx if dx > @@system_size / 2
      dy = (other.position[1] - self.position[1]).abs
      dy = @@system_size - dy if dy > @@system_size / 2
      Math.sqrt(dx * dx + dy * dy)
    end

    private
    def gaussian(mean = 0.0, stddev = 1.0)
      theta = 2 * Math::PI * rand
      rho = Math.sqrt(-2 * Math.log(1 - rand))
      scale = stddev * rho
      x = mean + scale * Math.cos(theta)
      y = mean + scale * Math.sin(theta)
      return x
    end
  end
end

=begin
- Input JSON format
{
  "system_size": 100.0,
  "interaction_radius": 1.0,
  "connectance": 1.0,
  "initial_num_species": 1,
  "t_max": 1000,
  "t_sample": 1,
  "_seed": 1234987201
}
=end

$logger = Logger.new('log.txt')
parameters = JSON.load( File.read('_input.json') )
parameters = Hash[ parameters.map {|key,val| [key.to_sym, val] } ]

$logger.info "parameters: #{parameters.inspect}"
Dir.chdir(File.dirname(__FILE__)) {
  simulator_version = `git describe --always`.chomp
  $logger.info "simulator_version: #{simulator_version}"
}

t_max = parameters[:t_max]
t_sample = parameters[:t_sample]
t_init = parameters[:t_init]
srand(parameters[:_seed])

sim = Simulator.new( parameters )

ns = File.open("num_species.dat", 'w')
pos = File.open("positions.dat", "w")

pbar = ProgressBar.create(total: (t_max+t_init)/t_sample, output: $stderr)
t_init.times do |t|
  sim.update
end

num_species_total = 0
t_max.times do |t|
   sim.update
   sim.count_up_indegree_hist
   sim.count_up_distance_hist
   num_species_total += sim.num_species
   if t % t_sample == 0
     ns.puts "#{t} #{sim.num_species}"; ns.flush
     sim.positions.each {|position| pos.puts "#{t/t_sample} #{position.join(' ')}" }
     pbar.increment
   end
end

File.open("_output.json", 'w') do |io|
  io.puts ({num_species: num_species_total.to_f / t_max }).to_json
end

File.open("lifetime.dat", 'w') do |io|
  sim.lifetime_hist.to_a.sort_by {|x| x[0]}.each do |lifetime, count|
    io.puts "#{lifetime} #{count}"
  end
end

File.open("lifetime_logbin.dat" , 'w') do |io|
  logbin_hist = {}
  sim.lifetime_hist.each_pair do |lifetime, count|
    key = 2 ** Math.log2(lifetime).to_i
    logbin_hist[key] = logbin_hist[key].to_i + count
  end
  logbin_hist.to_a.sort_by {|x| x[0]}.each do |l, count|
    io.puts "#{l} #{count.to_f/l}"
  end
end

File.open("indegree.dat", 'w') do |io|
  sim.indegree_hist.to_a.sort_by {|x| x[0]}.each do |indegree, count|
    io.puts "#{indegree} #{count}"
  end
end

File.open("distance.dat", 'w') do |io|
  sim.distance_hist.to_a.sort_by {|x| x[0]}.each do |bin_index, count|
    io.puts "#{bin_index} #{count}"
  end
end
